function task1(fullfill, reject) {
	console.log('task1 시작');
	setTimeout(function () {
		console.log('task1 끝');
		//fullfill('task1 결과');
		reject('task1 에러');
	}, 300);
}

function fullfilled(result) {
	console.log('fullfilled: ', result);
}
function rejected(err) {
	console.log('rejejcted: ', err);
}

new Promise(task1).then(fullfilled, rejected);