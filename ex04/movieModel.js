var mongoose = require('mongoose');
var url = 'mongodb://localhost/Movist';

mongoose.connect(url);

var conn = mongoose.connection;

conn.on('error', function(err){
	console.log('error: ', err);
});
conn.on('open', function(){
	console.log('open');
});

var MovieSchema = mongoose.Schema({
	title: String,
	director: String,
	year: Number,
	reviews: [String]
});

MovieSchema.methods.addReview = function (review) {
	this.reviews.push(review);
	return this.save();
};

var Movie = mongoose.model('Movie', MovieSchema);

module.exports = Movie;

// 초기화
// var movie1 = new Movie({
// 	title: '인터스텔라',
// 	director: '크리스토퍼 놀란',
// 	year: 2014
// });
//
// movie1.save(function(err, result, rows){
// 	if (err) {
// 		console.error('Error: ', err);
// 	}
// 	else {
// 		console.log('Success');
// 	}
// });
//
// Movie.create({
// 	title: '인터스텔라2',
// 	director: '크리스토퍼 놀란2',
// 	year: 2015
// }).then(function fulfilled(){
// 	console.log('Success');
// }, function rejected(err) {
// 	console.error('Error: ', err);
// });
//
// Movie.create({
// 	title: '인터스텔라2',
// 	director: '크리스토퍼 놀란2',
// 	year: 2016
// }, function (err, result){
// 	if (err) {
// 		console.error('Error: ', err);
// 	}
// 	else {
// 		console.log('Success');
// 	}
// });