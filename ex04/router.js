var express = require('express');
var router = express.Router();
var Movie = require('./movieModel');

router.get('/movies', showMovieList);
router.post('/movies', addMovie);
router.get('/movies/:movieId', showMovieDetail);
router.post('/movies/:movieId', addMovieReview);
router.put('/movies/:movieId', editMovie);
router.delete('/movies/:movieId', deleteMovie);

function deleteMovie(req, res, next){
	var movieId = req.params.movieId;

	Movie.findOneAndRemove({_id: movieId}).then(function fulfilled(result){
		console.log('삭제 성공');
		res.send({
			msg: 'success',
			id: result._id
		});
	}, function rejected(err){
		err.code = 500;
		next(err);
	});
}

function editMovie(req, res, next) {
	var movieId = req.params.movieId;
	var title = req.body.title;
	var director = req.body.director;
	var year = req.body.year;

	Movie.findById(movieId, function(err, doc){
		if (err) {
			err.code = 500;
			return next(err);
		}
		console.log('수정 성공');
		if (title) {
			doc.title = title;
		}
		if (director) {
			doc.director = director;
		}
		if (year) {
			doc.year = year;
		}

		doc.save().then(function fulfilled(result){
			console.log(result);
			res.send({
				msg: 'success',
				data: result
			});
		}, function rejected(err){
			err.code = 500;
			next(err);
		});
	});
}

function addMovie(req, res, next) {
	var title = req.body.title;
	var director = req.body.director;
	var year = req.body.year;

	var info = {
		title: title,
		director: director,
		year: year
	};

	var movie = new Movie(info);
	movie.save().then(function fulfilled(result){
		console.log(result);
		res.send({
			msg: 'success',
			id: result._id
		});
	}, function rejected(err){
		err.code = 500;
		next(err);
	});
}

function addMovieReview(req, res, next) {
	var movieId = req.params.movieId;
	var review = req.body.review;

	Movie.findById(movieId, function(err, doc) {
		if (err) {
			err.code = 500;
			return next(err);
		}

		doc.addReview(review).then(function fulfilled(result){
			console.log(result);
			res.send({
				msg: '리뷰 작성 success',
				id: result
			});
		}, function rejected(err){
			err.code = 500;
			next(err);
		});

		// doc.reviews.push(review);
		// doc.save().then(function fulfilled(result){
		// 	console.log(result);
		// 	res.send({
		// 		msg: '리뷰 작성 success',
		// 		id: result
		// 	});
		// }, function rejected(err){
		// 	err.code = 500;
		// 	next(err);
		// });
	});
}

function showMovieDetail(req, res, next) {
	var movieId = req.params.movieId;

	Movie.findById(movieId).exec(function (err, docs){
		if (err) {
			err.code = 500;
			return next(err);
		}
		console.log('성공');
		res.send(docs);
	});
}
function showMovieList (req, res, next) {
	Movie.find({}, {_id:1, title:1}).then(function fulfilled(docs){
		console.log('성공');
		var result = {
			count: docs.length,
			data: docs
		};
		res.send(result);
	}, function rejected(err){
		err.code = 500;
		next(err);
	});
}

module.exports = router;