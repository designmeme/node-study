var http = require('http');
var fs = require('fs');

var data = fs.readFileSync('./movieData.json');
var movieList = JSON.parse(data);

var server = http.createServer(function (req, res) {
	var method = req.method.toLowerCase();

	switch (method) {
		case 'get' :
			handleGetRequest(req, res);
			return;
		case 'post' :
			handlePostRequest(req, res);
			return;
		case 'put' :
			handlePutRequest(req, res);
			return;
		case 'delete' :
			handleDeleteRequest(req, res);
			return;
		default :
			res.statusCode = 404;
			res.end('잘못된 요청입니다.');
	}
}).listen(3000);

function handleGetRequest(req, res) {
	var url = req.url;
	if (url == '/movies') {
		var list = [];
		movieList.forEach(function(item) {
			list.push({id: item.id, title: item.title});
		});

		var result = {
			count: list.length,
			data: list
		}

		res.writeHead(200, {'content-type': 'application/json'});
		res.end(JSON.stringify(result));
	} else {
		var id = url.split('/')[2];
		var movie = null;

		movieList.forEach(function (item) {
			if (id == item.id) {
				movie = item;
				return;
			}
		});

		if (movie) {
			res.writeHead(200, {'content-type': 'application/json'});
			res.end(JSON.stringify(movie));
		} else {
			res.writeHead(404, {'content-type': 'application/json'});
			res.end(JSON.stringify({
				error: {
					code: 404,
					message: '영화 정보가 없습니다.'
				}
			}));
		}
	}
}