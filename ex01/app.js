var http = require('http');
var fs = require('fs');
var path = require('path');
var formidable = require('formidable');

var movieList = [];

http.createServer(function(req, res) {

	if (req.url == '/' && req.method.toLowerCase() == 'get') {
		showList(res);
	} else if (req.url.indexOf('/image') == 0 && req.method.toLowerCase() == 'get') {
		res.writeHead(200, {'content-type': 'image/jpeg'});
		fs.createReadStream(__dirname + req.url).pipe(res);

	} else if  (req.url == '/' && req.method.toLowerCase() == 'post') {
		addNewMovie(req, res);
	}

}).listen(3000);

function showList(res) {
	res.writeHead(200, {'content-type': 'text/html'});

	var body = '';
	body += '<!DOCTYPE html> <html lang="ko"> <head> <meta charset="UTF-8"> <title>Favorite Movie</title> </head> <body>';
	body += '<h1>Favorite Movie</h1>';
	body += '<ul>';

	movieList.forEach(function(item){
		body += '<li><img src="' + item.poster + '" alt="" width="100"> ' + item.title + '(' + item.director + ', ' + item.year + ')</li>';
	});

	body += '</ul>';
	body += '<h2>새 영화 입력</h2>';
	body += '<form action="." method="post" enctype="multipart/form-data">';
	body += '<p><label>제목 <input type="text" name="title"></label></p>';
	body += '<p><label>감독 <input type="text" name="director"></label></p>';
	body += '<p><label>연도 <input type="text" name="year"></label></p>';
	body += '<p><label>포스터 <input type="file" name="poster"></label></p>';
	body += '<input type="submit" value="올리기">';
	body += '</form></body></html>';

	res.end(body);
}

function addNewMovie(req, res) {
	var form = formidable.IncomingForm();
	form.uploadDir = __dirname + '/upload';

	form.parse(req, function(err, fields, files) {
		if (err) {
			return;
		}

		var title = fields.title;
		var director = fields.director;
		var year = fields.year;
		var poster = files.poster;

		var date = new Date();
		var url = 'image/image_' +  + date.getYear() + date.getMonth() + date.getMinutes() + date.getSeconds() + path.parse(poster.name).ext;
		var newPath = __dirname + '/' + url;

		fs.renameSync(poster.path, newPath);

		var info = {
			title: title,
			director: director,
			year: year,
			poster: url
		}

		movieList.push(info);

		res.statusCode = 302;
		res.setHeader('Location', '.');
		res.end();
	});

}