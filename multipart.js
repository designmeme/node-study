var fs = require('fs');
var pathUtil = require('path');
var http = require('http');
var formidable = require('formidable');

var uploadDir = __dirname + '/upload';
var imageDir = __dirname + '/image';

var paintList = [];

var server = http.createServer(function(req, res) {
	if (req.url == '/' && req.method.toLowerCase() == 'get') {
		showList(res);
	}
	// img 태그로 인한 요청
	else if (req.method.toLowerCase() == 'get' && req.url.indexOf('/image') == 0) {
		var path = __dirname + req.url;
		res.writeHead(200, {'content-type': 'image/jpeg'});
		fs.createReadStream(path).pipe(res);
	}
	// 업로드 요청
	else if (req.method.toLowerCase() == 'post') {
		addNewPaint(req, res);
	}
}).listen(3000);

function showList(res) {
	res.writeHead(200, {'content-type': 'text/html'});
	
	var body = '<html>';
	body += '<head><meta charset="UTF-8"></head>';
	body += '<body>';
	body += '<h3>Favorite Paint</h3>';
	body += '<ul>';
	paintList.forEach(function(item) {
		body += '<li>';
		if (item.image) {
			body += '<img src="' + item.image + '" style="height:100px">';
		}
		body += item.title;
		body += '</li>';
	});

	body += '</ul>';
	body += '<form action="." enctype="multipart/form-data" method="post">';
	body += '<div><label>작품 이름: <input type="text" name="title"></label></div>';
	body += '<div><label>작품 이미지: <input type="file" name="image" value="작품 파일 선택"></label></div>';
	body += '<input type="submit" value="업로드">';
	body += '</form>';
	body += '</body></html>';

	res.end(body);
}

function addNewPaint(req, res) {
	var form = formidable.IncomingForm();
	form.uploadDir = uploadDir;

	form.parse(req, function(err, fields, files) {
		var title = fields.title;
		var image = files.image;

		var date = new Date();
		var newImageName = 'image_' + date.getHours() + date.getMinutes() + date.getSeconds();
		var ext = pathUtil.parse(image.name).ext;

		var newPath = __dirname + '/image/' + newImageName + ext;

		fs.renameSync(image.path, newPath);

		var url = 'image/' + newImageName + ext;

		var info = {
			title : title,
			image: url
		};

		paintList.push(info);

		res.statusCode = 302;
		res.setHeader('Location', '.');
		res.end('success');
	});
}