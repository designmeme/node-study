var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var ejs = require('ejs');
var router = require('./router');

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/', router);

app.listen(3000, function () {
	console.log('app start');
});