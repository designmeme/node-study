var express = require('express');
var router = express.Router();
var data = [
	{
		"movieId":0,
		"title":"Avata",
		"director":"제임스 카메론",
		"year":2009,
		"reviews":[]
	},
	{
		"movieId":1,
		"title":"Starwars1",
		"director":"조지 루카스",
		"year":1977,
		"reviews":[]
	},
	{
		"movieId":2,
		"title":"Interstella",
		"director":"크리스토퍼 놀란",
		"year":2014,
		"reviews":[]
	}
];

router.get('/', getMovieList);
router.get('/detail/:id', getMovieDetail);
router.post('/detail/:id', updateMovieDetail);

function getMovieList (req, res) {
	res.render('movieList', {
		data: data
	});
}
function getMovieDetail (req, res) {
	var movie;

	data.forEach(function(item) {
		if (item.movieId == req.params.id) {
			movie = item;
			return;
		}
	});

	res.render('movieDetail', {
		data: movie
	});
}
function updateMovieDetail (req, res) {
	var movie;

	data.forEach(function(item) {
		if (item.movieId == req.params.id) {
			movie = item;
			return;
		}
	});

	movie.reviews.push(req.body.review);

	res.redirect('/detail/' + movie.movieId);
}


module.exports = router;