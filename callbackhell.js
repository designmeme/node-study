function task1(callback) {
	console.log('task1 시작');
	setTimeout(function () {
		console.log('task1 끝');
		//callback();
	}, 300);
}

function task2(callback) {
	console.log('task2 시작');
	setTimeout(function () {
		console.log('task2 끝');
		//callback();
	}, 200);
}

task1();
task2();