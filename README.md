# 하우투메리 노드스터디
강의자료
- SK플래닛 상생혁신센터 온라인강의 'Node.js 프로그래밍'
- https://oic.skplanet.com

링크
- https://nodejs.org/
- https://nodejs.org/dist/latest-v5.x/docs/api/

## 강의노트

### [1강] Node.js의 개요
- 노드 역사, 장점 등 소개
- 노드 설치하기

### [2강] Node.js의 기본 모듈1
  - `process`
    - `.arch`
    - `.argv`
    - `.env`
    - Event: `exit`
    - Event: `uncaughtException`
  - Timers
    - `setTimeout(callback, delay[, arg][, ...])`
    - `setInterval(callback, delay[, arg][, ...])`
    - `clearInterval(intervalObject)`
  - `console`
    - 로그
      - `.info([data][, ...])`
      - `.log([data][, ...])`
      - `.warn([data][, ...])`
      - `.error([data][, ...])`
    - 커스텀 콘솔
      - `require('console').Console` || `console.Console`
      - `new Console(stdout[, stderr])`
    - 실행 시간 측정
      - `.time(label)`
      - `.timeEnd(label)`
  - Util
    - `require('util')`
    - 문자열 포맷
      - `.format(format[, ...])`
    - 상속
      - `.inherits(constructor, superConstructor)`
  - Events
    - Class: EventEmitter
      - `emitter.addListener(event, listener)`
      - `emitter.on(event, listener)`
      - `emitter.once(event, listener)`
      - `emitter.removeListener(event, listener)`
      - `emitter.removeAllListeners([event])`
      - `emitter.getMaxListeners()`
      - `emitter.setMaxListeners(n)`
      - `emitter.emit(event[, arg1][, arg2][, ...])`

### [3강] Node.js의 기본 모듈2
- Path
  - `require('path')`
- File System
  - `require('fs')`
- Buffer

### [4강] Node.js의 기본 모듈3
- Stream
  - 콘솔 입력/출력
  - 파일 읽기/쓰기
  - 서버/클라이언트 - 데이터 전송
- URL
  - `require('url')`
  - 참조: urlencode
- Query String
  - `require('querystring')`
- Cluster
  - `require('cluster')`
  - 참조: pm2

### [5강] npm을 이용한 모듈 관리
- npm
- 패키지 정보
- 확장 모듈 nodemon
- 커스텀 모듈 만들기

### [6강] 흐름 제어 모듈
- 콜백과 콜백 헬
- Async : flow control
- Promise : chain

### [7강] HTTP 통신
- HTTP 통신
- HTTP 요청 - 응답
- HTTP 모듈
  - `require('http')`
- HTTP 서버 - 클라이언트

### [8강] HTTP 서버
- HTTP 서버 작성
- HTTP 요청 분석
- HTTP 응답 메시지 작성
- 정적 파일 요청과 서비스 제공하는 서버 작성

### [9강] HTTP POST 요청 처리 1
- HTTP POST 요청 처리
- 중복 POST 요청 방지 : PGR(Post-Redirect-Get) 패턴
- HTTP 클라이언트 확장 프로그램 Postman

### [10강] HTTP POST 요청 처리 2
- 멀티파트 요청
- 멀티파트 관련 모듈
  - formidable
  - multer

### [11강] 모바일 서버
- JSON
- XML
  - libxmljs 모듈
  - jstoxml 모듈

### [12강] REST 서버
- REST 서비스

### [13강] Express
- Express 프레임워크
- Express 미들웨어
 - 일반적인 미들웨어 동작순서 : 파비콘 -> 로그 -> 정적파일 -> 서비스 -> 에러
- 정적 파일 서비스 미들웨어

### [14강] Express 라우팅
- 라우팅
- 에러 처리 미들웨어

### [15강] Express 써드파티 미들웨어
- 파비콘 미들웨어
  - serve-favicon 모듈
- 로그 미들웨어
  - morgan 모듈
  - winston 모듈
    - transforts 관련 모듈 별도 : ex) daily rotate file 모듈
- body-parser 모듈
- 메소드 오버라이드
  - method-override 모듈

### [16강] Express 템플릿 엔진
- ejs
- jade

### [17강] Node.js와 MySQL
- MySQL
- Sequelize

### [18강] Node.js와 MongoDB
- MongoDB
- mongodb 모듈
- mongoose 모듈

### [19강] 소켓을 이용한 실시간 서비스
- TCP 실시간 서비스
  - net 모듈
- UDP 실시간 서비스
  - dgram 모듈

### [20강] Socket.IO 를 이용한 실시간 웹 서비스
- socket.io
- 네임스페이스와 룸

### [21강] 인증
- 보안과 인증
- 쿠키
  - cookie-parser 모듈
  - 서명 쿠키
- 세션
  - express-session 모듈
  - connect-mongo 모듈
  - connect-redis 모듈

### [22강] Passport를 이용한 인증
- passport 모듈
- Local Auth
- Facebook OAuth

### [23강] 보안
- 해시
  - crypto 모듈
- 암호화
- 보안서버

### [24강] 디버깅과 프로세스 관리
- 디버깅 모드 `--debug`
  - node-inspector 모듈
- 테스트
  - assert, should 모듈
  - mocha 프레임워크
- 프로세스 관리
  - forever 모듈