function task1(callback) {
	console.log('task1 시작');
	setTimeout(function () {
		console.log('task1 끝');
		callback(null, 'task1 ok');
		//callback('error');
	}, 300);
}

function task2(callback) {
	console.log('task2 시작');
	setTimeout(function () {
		console.log('task2 끝');
		callback(null, 'task2 결과');
	}, 200);
}

var async = require('async');
async.series([task1, task2], function (err, results) {
	if (err) {
		console.log('에러났다', err);
		return;
	}
	console.log('비동기 모두 종료 ', results);
});
