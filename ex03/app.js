var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');

var app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': false}));

app.use(require('./router.js'));
app.use(cusError);

app.listen(3000);

function cusError(err, req, res, next) {
	res.status(err.code).send({msg: err.message});
}
