var mysql = require('mysql');
var dbConfig = {
	host: 'localhost',
	user: 'node',
	password: '1234',
	port: 3306,
	multipleStatements: true,
	database: 'node'
};

var pool =  mysql.createPool(dbConfig);

module.exports = pool;

// 초기화
// pool.getConnection(function(err, conn) {
// 	if (err) {
// 		console.error('Connection Error: ', err);
// 		return;
// 	}
//
// 	var fs = require('fs');
// 	var sqls = fs.readFileSync('./initialData.sql', 'utf-8');
//
// 	conn.query(sqls, function(err, results){
// 		if (err) {
// 			console.error('Initialdata error', err);
// 			return;
// 		}
//
// 		console.log('init success');
// 	});
//
// 	console.log('success');
// });